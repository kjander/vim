set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" You Complete Me Plugin
Plugin 'Valloric/YouCompleteMe'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let g:ycm_show_diagnostics_ui = 0

"Search for ctags in current and higher directories
set tags=./tags;,tags;

"Syntax highlighting for javascript embeded in html
au BufRead *.html set filetype=htmlm4

set expandtab tabstop=4 softtabstop=4 "Insert tabs as 4 spaces
set shiftwidth=4  autoindent "Autoindent 4 space tabs

"Highlight columns 80 and 100
set colorcolumn=80,100
hi ColorColumn guibg=#2d2d2d ctermbg=238

"Highlight search matches
set hlsearch

let mapleader=" "

"Split window in given direction, ready for search for word below cursor
"E.g. Cursor over function name, split into new window.
"Name is now highlighted so you can tag search g] to find the functions
"declaration.
nnoremap <Leader>h *N:vsp<CR>:wincmd x<CR>
nnoremap <Leader>l *N:vsp<CR>:wincmd w<CR>
nnoremap <Leader>k *N:sp<CR>:wincmd x<CR>
nnoremap <Leader>j *N:sp<CR>:wincmd w<CR>

"Open explorer
nnoremap <F2> :Explore<CR>

"Close current window
nnoremap <Leader>w <ESC>:q<CR>

"Remove highlighting and disable spell check
nnoremap <F4> :noh<CR>:set nospell<CR><ESC>

"Save/write file
inoremap <c-k> <ESC>:w<CR>
nnoremap <c-k> <ESC>:w<CR>

"Remove all trailing whitespace
nnoremap <F3> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

"Movement
nnoremap <S-k> {
nnoremap <S-j> }

"Spell checker
set spelllang=en_au
hi SpellLocal ctermbg=011
noremap <F5> <ESC>:set spell! <CR>

"File searcher
vnoremap <F9> y:vimgrep /<C-R>"/g `git ls-files` \| cwin<CR>
nnoremap <F9> viwy:vimgrep /<C-R>"/g `git ls-files` \| cwin<CR>
vnoremap <c-F9> y:vimgrep /<C-R>"/g **/* \| cwin
nnoremap <c-F9> viwy:vimgrep /<C-R>"/g **/* \| cwin

" Enable taglist plugin
filetype plugin on

"Open tagged definition in new tab
noremap <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>

noremap <Leader>/ :tselect /

"Toggle tag (function, class, variable) list
noremap <F6> <ESC>:TlistToggle <CR>

"Move between tag matches
noremap <F7> :tNext<CR>
noremap <F8> :tnext<CR>
